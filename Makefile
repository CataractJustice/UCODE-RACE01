MATPART=part_of_the_matrix
all: $(MATPART)

$(MATPART): src/*.c inc/*.h
	clang -std=c11 -Wall -Wextra -Werror -Wpedantic src/*.c -I inc -o part_of_the_matrix

clean:
	rm part_of_the_matrix

uninstall: clean

reinstall: uninstall all
