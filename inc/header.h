#ifndef HEADER_H
#define HEADER_H
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>

enum e_unknown_operand {
	FIRST,
	SECOND,
	RESULT
};

int numbergen(const char* s, int n);
void printequation (int a, char o, int b, int c);
int count_combinations(const char* str);
int count_unknown_digits (const char* str);
bool compare(int n, const char* s);
void solve_and_print_sub(int a, int b, enum e_unknown_operand unknown, const char* unknownstr);
void solve_and_print_add(int a, int b, enum e_unknown_operand unknown, const char* unknownstr);
void solve_and_print_mul(int a, int b, enum e_unknown_operand unknown, const char* unknownstr);
void solve_and_print_div(int a, int b, enum e_unknown_operand unknown, const char* unknownstr);
void solve_and_print(const char* a, const char* b, const char* c, char op);
void mx_printchar(char c);
void mx_printint(int num);
void mx_printerr(const char* str);
char* parse_operand(char* str);
char getop(const char* str);
#endif
