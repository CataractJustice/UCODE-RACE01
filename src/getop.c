#include "header.h"

char getop(const char* str) {
	const char* cursor = str;

	while(*cursor == ' ') {
		cursor++;
	}
	
	char op;
	
	if(*cursor == '+' || *cursor == '-' || *cursor == '*' || *cursor == '/' || *cursor == '?') {
		op = *cursor;
		cursor++;
	}
	else {
		return 0;
	}
	
	while(*cursor) {
		if(*cursor != ' ')
			return 0;
		cursor++;
	}
	
	return op;
}
