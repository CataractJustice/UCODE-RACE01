#include "header.h"

char* parse_operand(char* str) {
	char* begin = str;
	
	while(*begin == ' ') {
		begin++;
	}
	
	char* cursor = begin;
	
	while((*cursor >= '0' && *cursor <= '9') || *cursor == '?') {
		cursor++;
		if(*cursor == ' ') {
			*cursor = 0;
			cursor++;
		}
	}
	
	while(*cursor) {
		if(*cursor != ' ')
			return 0;
		cursor++;
	}
	
	return begin;
}
