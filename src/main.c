#include "header.h"

int main(int argc, char** argv) {
	
	if(argc != 5) {
		mx_printerr("usage: ./part_of_the_matrix [operand1] [operation] [operand2] [result]\n");
		exit(0);
	}
	
	char op = getop(argv[2]);
	
	if(!op) {
		mx_printerr("Invalid operation: ");
		mx_printerr(argv[2]);
		mx_printerr("\n");
		exit(-1);
	}
	
	char* operand1 = parse_operand(argv[1]);
	char* operand2 = parse_operand(argv[3]);
	char* result = parse_operand(argv[4]);
	
	if(!operand1 || !operand2 || !result) {
		mx_printerr("Invalid operand: ");
		if(!operand1)
			mx_printerr(argv[1]);
		else
		if(!operand2)
			mx_printerr(argv[3]);
		else
		if(!result)
			mx_printerr(argv[4]);
			
		mx_printerr("\n");
		exit(-2);
	}
	
	if(op == '?') {
		solve_and_print(operand1, operand2, result, '+');
		solve_and_print(operand1, operand2, result, '-');
		solve_and_print(operand1, operand2, result, '*');
		solve_and_print(operand1, operand2, result, '/');
	}
	else
		solve_and_print(operand1, operand2, result, op);
}

