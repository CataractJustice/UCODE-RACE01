#include "header.h"

void solve_and_print_sub(int a, int b, enum e_unknown_operand unknown, const char* unknownstr) {

	switch(unknown) {
		case FIRST:
			if(compare(a + b, unknownstr))
				printequation(a + b, '-', a, b);
		break;
		
		case SECOND:
			if(compare(a - b, unknownstr))
				printequation(a, '-', a - b, b);
		break;
		
		case RESULT:
			if(compare(a - b, unknownstr))
				printequation(a, '-', b, a - b);
		break;
		
		default:
		
		break;
	}
}
