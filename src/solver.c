#include "header.h"

void solve_and_print_op(char op, int a, int b, enum e_unknown_operand unknown, const char* unknownstr) {
	switch(op) {
		case '+':
			solve_and_print_add(a, b, unknown, unknownstr);
		break;
		
		case '-':
			solve_and_print_sub(a, b, unknown, unknownstr);
		break;
		
		case '*':
			solve_and_print_mul(a, b, unknown, unknownstr);
		break;
		
		case '/':
			solve_and_print_div(a, b, unknown, unknownstr);
		break;
	}
}

void solve_and_print(const char* a, const char* b, const char* c, char op) {
	
	int a_combinations = count_combinations(a);
	int b_combinations = count_combinations(b);
	int c_combinations = count_combinations(c);
	
	int unknown_operands[3] = {(a_combinations > 1), (b_combinations > 1), (c_combinations > 1)};
	int unknown_operands_count = unknown_operands[0] + unknown_operands[1] + unknown_operands[2];
	
	
	const char* solver_a;
	const char* solver_b;
	const char* unknownstr;
	enum e_unknown_operand unknown;
	
	switch (unknown_operands_count) {
	
	case 0:
		
		solve_and_print_op(op, numbergen(a, 0), numbergen(b, 0), RESULT, c);
		
	break;
		
	case 1:
	
		if(unknown_operands[0]) {
			unknown = FIRST;
			solver_a = b;
			solver_b = c;
			unknownstr = a;
		}
		
		if(unknown_operands[1]) {
			unknown = SECOND;
			solver_a = a;
			solver_b = c;
			unknownstr = b;
		}
		
		if(unknown_operands[2]) {
			unknown = RESULT;
			solver_a = a;
			solver_b = b;
			unknownstr = c;
		}
		
		solve_and_print_op(op, numbergen(solver_a, 0), numbergen(solver_b, 0), unknown, unknownstr);
		
	break;
		
	case 2:
		
		
	
		if(!unknown_operands[0]) {
			unknown = RESULT;
			solver_a = a;
			solver_b = b;
			unknownstr = c;
			
			for(int i = 0; i < b_combinations; i++) {
				solve_and_print_op(op, numbergen(solver_a, 0), numbergen(solver_b, i), unknown, unknownstr);
			}
			break;
		}
		
		if(!unknown_operands[1]) {
			unknown = RESULT;
			solver_a = a;
			solver_b = b;
			unknownstr = c;
		}
		
		if(!unknown_operands[2]) {
			unknown = SECOND;
			solver_a = a;
			solver_b = c;
			unknownstr = b;
		}
		
		for(int i = 0; i < a_combinations; i++) {
			solve_and_print_op(op, numbergen(solver_a, i), numbergen(solver_b, 0), unknown, unknownstr);
		}
		
	break;
		
	case 3:
		
		for(int i = 0; i < a_combinations; i++) {
			for(int j = 0; j < b_combinations; j++) {
				solve_and_print_op(op, numbergen(a, i), numbergen(b, j), RESULT, c);
			}
		}
		
	break;
	}
	
}
