#include "header.h"

/*
#include <stdio.h>

int numbergen(const char* s, int n);

int main() {
	for(int i = 0; i <= 99; i++) {
		printf("%d\n", numbergen("?1?", i));
	}
}
*/

int numbergen(const char* s, int n) {
	const char* cursor = s;
	int result = 0;
	int mul = 1;
	
	while(*(++cursor));
	cursor--;
	
	while(cursor >= s) {
		if(cursor == s) {
			if(*cursor == '-') {
				return -result;
			}
		}
	
		int digit;
		
		if(*cursor == '?') {
			digit = n % 10;
			n /= 10;	
		}
		else {
			digit = *cursor - '0';
		}
		
		result += digit * mul;
		mul *= 10;
		
		cursor--;
	}
	return result;
}


