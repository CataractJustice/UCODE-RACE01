#include "header.h"

void solve_and_print_mul(int a, int b, enum e_unknown_operand unknown, const char* unknownstr) {

	switch(unknown) {
		case FIRST:
			if(a == 0) {
				if(b != 0)
					return;
				int combs = count_combinations(unknownstr);
				for(int i = 0; i < combs; i++) {
					printequation(numbergen(unknownstr, i), '*', a, b);
				}
			}
			else {
				if(compare(b / a, unknownstr) && b % a == 0)
					printequation(b / a, '*', a, b);
			}
		break;
		
		case SECOND:
			if(a == 0) {
				if(b != 0)
					return;
				int combs = count_combinations(unknownstr);
				for(int i = 0; i < combs; i++) {
					printequation(a, '*', numbergen(unknownstr, i), b);
				}
			}
			else {
				if(compare(b / a, unknownstr) && b % a == 0)
					printequation(a, '*', b / a, b);
			}
		break;
		
		case RESULT:
			if(compare(a * b, unknownstr))
				printequation(a, '*', b, a * b);
		break;
		
		default:
		
		break;
	}
}
