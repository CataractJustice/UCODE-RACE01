#include "header.h"

void mx_printint(int num) {
	if(num == 0) {
		mx_printchar('0');
	}
	int n = num > 0 ? num : -num;
	int length = 0;
	char buff[32];
	
	while (n)
	{
		buff[length] = '0' + n % 10;
		n /= 10;
		length++;
	}
	
	if(num < 0) {
		buff[length] = '-';
		length++;
	}
	
	for(int i = 1; i <= length; i++)
		mx_printchar(buff[length - i]);
}

