#include "header.h"
#include <stdio.h>
/*
bool compare(int n, const char* s);

int main() {
	printf("%d\n", compare(-123, "-?2?"));
	printf("%d\n", compare(124, "-?2?"));
	printf("%d\n", compare(133, "?2?"));
}
*/

bool compare(int n, const char* s) {

	const char* cursor = s;
	int sign = n < 0 ? -1 : 1;
	if(sign == -1 && *s != '-') {
		return false;
	}
	
	
	
	while(*(++cursor));
	cursor--;
	
	while(cursor >= s) {
		if(cursor == s) {
			if(*cursor == '-') {
				if(sign == 1)
					return false;
				else
					break;
			}
		}
	
		if(('0' + ((n > 0 ? n : -n) % 10)) != *cursor && *cursor != '?') {
			return false;
		}
		n /= 10;
		cursor--;
	}
	
	if(n) {
		return false;
	}
	return true;
}


