#include "header.h"
int count_combinations(const char* str) {
	int q = count_unknown_digits(str);
	int n = 1;
	
	for(int i = 0; i < q; i++)
		n *= 10;
		
	return n;
}
