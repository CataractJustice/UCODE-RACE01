#include "header.h"
int count_unknown_digits (const char* str) {
	int unknowns = 0;
	
	while(*str) {
		if(*str == '?')
			unknowns++;
		str++;
	}
	
	return unknowns;
}
