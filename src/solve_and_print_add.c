#include "header.h"

void solve_and_print_add(int a, int b, enum e_unknown_operand unknown, const char* unknownstr) {

	switch(unknown) {
		case FIRST:
			if(compare(b - a, unknownstr))
				printequation(b - a, '+', a, b);
		break;
		
		case SECOND:
			if(compare(b - a, unknownstr))
				printequation(a, '+', b - a, b);
		break;
		
		case RESULT:
			if(compare(a + b, unknownstr))
				printequation(a, '+', b, a + b);
		break;
		
		default:
		
		break;
	}
}
