#include "header.h"

void solve_and_print_div(int a, int b, enum e_unknown_operand unknown, const char* unknownstr) {

	switch(unknown) {
		case FIRST:
			if(a == 0)
				return;
			for(int i = 0; i < a; i++) {
				if(compare(a * b + i, unknownstr))
					printequation(a * b + i, '/', a, b);
			}
		break;
		
		case SECOND:
			if(b == 0)
				return;
			for(int i = 0; compare(a / b - i, unknownstr); i++) {
					printequation(a, '/', a / b - i, b);
			}
		break;
		
		case RESULT:
			if(b == 0)
				return;
			if(compare(a / b, unknownstr))
				printequation(a, '/', b, a / b);
		break;
		
		default:
		
		break;
	}
}
